package jp.alhinc.yoshida_ren.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;


public class CalculateSales {

	public static void main(String[] args) {
		
//		支店番号と支店名を入れるマップ
		Map<String, String> shops = new LinkedHashMap<String, String>();
		
//		支店番号と売上を入れるマップ
		Map<String, Long> sales = new LinkedHashMap<String, Long>();
		
//		コマンドライン引数から支店定義ファイルを指定
		File file = new File(args[0], "branch.lst");
//		ファイルを読み込む
		InputStreamReader reader = null;
		BufferedReader buff = null;
		try {
			reader = new InputStreamReader(new FileInputStream(file));
			buff = new BufferedReader(reader);
			String line;
			
			while((line = buff.readLine()) != null) {
				String[] value = line.split(",");
//				支店番号が数字三桁であるかどうかをチェック
				if(! value[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
//				支店の名前にコンマと改行が含まれていないかを配列の大きさでチェック
				int valueSize =value.length;
				if(valueSize != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
//				shopsに支店コードと支店名を入れる
				shops.put(value[0], value[1]);
//				salesに支店コードと売上収集用の数字を入れる
				Long sale = 0L;
				sales.put(value[0], sale);
			}
		}catch(FileNotFoundException e) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}catch(IOException error) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			try {
				if(buff != null) {
					buff.close();
				}
			}catch(IOException error) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}
		
//		FilenameFilterを使い、拡張子とファイル名でフィルターを作る
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				
				if(str.matches("[0-9]{8}.rcd")) {
					return true;
				}else {
					return false;
				}
			}
		};
//		作ったフィルターを使い、ディレクトリ内のファイルを検索する
		File[] files = new File(args[0]).listFiles(filter);
		
		if(files != null) {
			
			String fileName = files[0].getName();
			String firstName = fileName.substring(0, fileName.lastIndexOf("."));
			int firstFileName = Integer.valueOf(firstName);
			
			for(int i = 0; i < files.length; ++i) {
				if(files[i].isFile()) {
					String fileNames = files[i].getName();
					String names = fileNames.substring(0, fileNames.lastIndexOf("."));
					int fileNumber = Integer.valueOf(names);
					
					if(fileNumber != i + firstFileName) {
						System.out.println("売上ファイル名が連番になっていません");
						return;
					}
					InputStreamReader reader2 = null;
					BufferedReader buff2 = null;
					try {
						reader2 = new InputStreamReader(new FileInputStream(files[i]));
						buff2 = new BufferedReader(reader2);
						
						String code = buff2.readLine();
						String cash = buff2.readLine();
						String errorLine = buff2.readLine();
						
						if(errorLine != null) {
							System.out.println(files[i].getName() + "のフォーマットが不正です");
							return;
						}
						if(! code.matches("[0-9]{3}")) {
							System.out.println(files[i].getName() + "のフォーマットが不正です");
							return;
						}
						if(! cash.matches("[0-9]{1,}")) {
							System.out.println(files[i].getName() + "のフォーマットが不正です");
							return;
						}
						if(sales.get(code) == null) {
							System.out.println(files[i].getName() + "の支店コードが不正です");
							return;
						}
						Long cashData = Long.parseLong(cash);
						sales.put(code, sales.get(code) + cashData);
						
						Long limit = 10000000000L;
						if(sales.get(code) >= limit) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
					}catch(IOException error) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}finally {
						try {
							if(buff2 != null) {
								buff2.close();
							}
						}catch(IOException error) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}else {
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}
			}
		}else {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
//		branch.outに支店コード、支店名、合計金額を出力する
		try {
			File salesFile = new File(args[0], "branch.out");
			FileWriter writer = new FileWriter(salesFile);
			BufferedWriter buffered = new BufferedWriter(writer);
			
//			mapのキーをキーセットで回し、対応するmapの値とsalesの値を書き込む
			for(String keyList : shops.keySet()) {
				buffered.write(keyList + "," + shops.get(keyList) + "," + sales.get(keyList));
				buffered.newLine();
			}
			buffered.close();
		}catch(IOException error) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}


